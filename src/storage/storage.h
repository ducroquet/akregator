/*
    This file is part of Akregator.

    SPDX-FileCopyrightText: 2005 Stanislav Karchebny <Stanislav.Karchebny@kdemail.net>
    SPDX-FileCopyrightText: 2005 Frank Osterfeld <osterfeld@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later WITH Qt-Commercial-exception-1.0
*/

#pragma once

#include "akregator_export.h"

#include <QObject>
#include <memory>

#include "feedstorage.h"

class QSqlDatabase;

namespace Akregator
{
namespace Backend
{
class AKREGATOR_EXPORT Storage : public QObject
{
    Q_OBJECT
public:
    Storage();
    virtual ~Storage();

    /**
     * Open storage and prepare it for work.
     * @return true on success.
     */
    bool open(bool autoCommit = false);

    /**
     * Commit changes made in feeds and articles, making them persistent.
     * @return true on success.
     */
    bool commit();

    /**
     * Rollback changes made in feeds and articles, reverting to last committed values.
     * @returns true on success.
     */
    bool rollback();

    /**
     * Closes storage, freeing all allocated resources. Called from destructor, so you don't need to call it directly.
     * @return true on success.
     */
    void close();

    /**
     * @return Article archive for feed at given url.
     */
    FeedStorage *archiveFor(const QString &url);
    const FeedStorage *archiveFor(const QString &url) const;

    bool autoCommit() const;

    void storeFeedList(const QString &opmlStr);
    QString restoreFeedList() const;

    void markDirty();

    QSqlDatabase database();

private:
    class StoragePrivate;
    std::unique_ptr<StoragePrivate> const d;
};
} // namespace Backend
} // namespace Akregator
