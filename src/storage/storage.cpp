/*
    This file is part of Akregator.

    SPDX-FileCopyrightText: 2005 Stanislav Karchebny <Stanislav.Karchebny@kdemail.net>
    SPDX-FileCopyrightText: 2005 Frank Osterfeld <osterfeld@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later WITH Qt-Commercial-exception-1.0
*/
#include "storage.h"

#include <QMap>
#include <QString>
#include <QStringList>
#include <QTimer>

#include <QDateTime>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <chrono>

using namespace std::chrono_literals;

class Akregator::Backend::Storage::StoragePrivate
{
public:
    StoragePrivate()
    {
    }

    Storage *parent;
    QSqlDatabase db;
    bool autoCommit;
    QTimer *autoCommitTimer;
    Akregator::Backend::FeedStorage *createFeedStorage(const QString &url);
    QHash<QString, Akregator::Backend::FeedStorage *> feeds;
};

QString defaultArchivePath()
{
    const QString ret = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + QStringLiteral("/akregator");
    QDir().mkpath(ret);
    return ret;
}

Akregator::Backend::Storage::Storage()
    : d(new StoragePrivate)
{
    d->parent = this;

    d->autoCommitTimer = new QTimer(this);
    d->autoCommitTimer->setInterval(3000); // Commit each 3s. at most
    d->autoCommitTimer->setSingleShot(true); // markDirty will start this timer
    QObject::connect(d->autoCommitTimer, &QTimer::timeout, this, [this]() {
        this->commit();
        this->d->db.transaction();
    });

    qDebug() << "Initializing Storage with SQLITE";
    d->db = QSqlDatabase::addDatabase(QLatin1String("QSQLITE"));
    d->db.setDatabaseName(defaultArchivePath() + QStringLiteral("/archive.sqlite"));
}

Akregator::Backend::FeedStorage *Akregator::Backend::Storage::StoragePrivate::createFeedStorage(const QString &url)
{
    if (feeds.contains(url)) {
        return feeds[url];
    }
    int feed_id;

    // Try to find the current feed_id. If it does not exists, insert a new row in feed.
    QSqlQuery q(db);
    q.prepare(QLatin1String("SELECT id FROM feed WHERE url = ?"));
    q.addBindValue(url);
    if (!q.exec()) {
        // TODO : forward error somehow ?
        return nullptr;
    }
    if (q.next()) {
        feed_id = q.value(0).toInt();
    } else {
        q.prepare(QLatin1String("INSERT INTO feed(url, last_fetch) VALUES (?, datetime('now'));"));
        q.addBindValue(url);
        if (!q.exec()) {
            // TODO : forward error somehow ?
            return nullptr;
        }
        feed_id = q.lastInsertId().toInt();
    }
    auto new_feed = new FeedStorage(feed_id, parent);
    feeds.insert(url, new_feed);
    return new_feed;
}

Akregator::Backend::FeedStorage *Akregator::Backend::Storage::archiveFor(const QString &url)
{
    return d->createFeedStorage(url);
}

const Akregator::Backend::FeedStorage *Akregator::Backend::Storage::archiveFor(const QString &url) const
{
    return d->createFeedStorage(url);
}

Akregator::Backend::Storage::~Storage()
{
    if (d->autoCommit)
        this->commit();
    d->autoCommitTimer->deleteLater();
    for (auto *feed : d->feeds.values())
        delete (feed);

    close();
}

bool Akregator::Backend::Storage::open(bool autoCommit)
{
    d->autoCommit = autoCommit;
    if (!d->db.open(/* TODO : user, password */)) {
        return false;
    }
    // TODO Versioning ?
    QStringList tables = d->db.tables(QSql::TableType::Tables);
    if (!tables.contains(QStringLiteral("opml"))) {
        QSqlQuery(QStringLiteral("CREATE TABLE opml(data text);"), d->db).exec();
        markDirty();
    }
    if (!tables.contains(QStringLiteral("feed"))) {
        QSqlQuery(QStringLiteral(
                      "CREATE TABLE feed(id integer primary key, url text unique not null, unread integer not null default 0, last_fetch timestamp not null);"),
                  d->db)
            .exec();
        markDirty();
    }
    if (!tables.contains(QStringLiteral("article"))) {
        QSqlQuery(QStringLiteral("CREATE TABLE article(feed_id integer not null, guid text not null, title text, hash integer, guid_is_hash boolean, "
                                 "guid_is_permalink boolean, description text, link text, status integer, publication_date timestamp, enclosure_url text, "
                                 "enclosure_type text, enclosure_length integer, author_name text, author_url text, author_email text, content text);"),
                  d->db)
            .exec();
        QSqlQuery(QStringLiteral("CREATE UNIQUE INDEX article_feed_guid ON article(feed_id, guid);"), d->db).exec();
        QSqlQuery(QStringLiteral("CREATE INDEX article_feed_unread ON article(feed_id, guid) WHERE (NOT(status & 8));"), d->db).exec();
        markDirty();
    }

    d->db.transaction();
    return true;
}

bool Akregator::Backend::Storage::autoCommit() const
{
    return d->autoCommit;
}

void Akregator::Backend::Storage::close()
{
    d->db.close();
}

bool Akregator::Backend::Storage::commit()
{
    bool result = d->db.commit();
    this->d->db.transaction();
    return result;
}

bool Akregator::Backend::Storage::rollback()
{
    return d->db.rollback();
}

void Akregator::Backend::Storage::markDirty()
{
    if (d->autoCommit) {
        d->autoCommitTimer->stop();
        d->autoCommitTimer->start();
    }
}

void Akregator::Backend::Storage::storeFeedList(const QString &opmlStr)
{
    QSqlQuery(QStringLiteral("DELETE FROM opml;"), d->db).exec();
    QSqlQuery q(d->db);
    q.prepare(QLatin1String("INSERT INTO opml(data) VALUES (?);"));
    q.addBindValue(opmlStr);
    q.exec();
    // This asserts it worked.
}

QString Akregator::Backend::Storage::restoreFeedList() const
{
    QSqlQuery q(QStringLiteral("SELECT data FROM opml;"), d->db);
    q.exec();
    if (q.next())
        return q.value(0).toString();
    else
        return QString();
}

QSqlDatabase Akregator::Backend::Storage::database()
{
    return d->db;
}

#include "moc_storage.cpp"
