/*
    This file is part of Akregator.

    SPDX-FileCopyrightText: 2005 Frank Osterfeld <osterfeld@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later WITH Qt-Commercial-exception-1.0
*/

#include "feedstorage.h"
#include "storage.h"

#include <Syndication/DocumentSource>
#include <Syndication/Feed>
#include <Syndication/Item>

#include <QDateTime>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>

namespace
{

/// Simple helpers to use the Qt SQL database API

#define DUMP_SQL 0

template<typename T>
T simpleQuery(const QSqlDatabase &db, const char *query, const T &default_value, const std::initializer_list<QVariant> &parameters)
{
    QSqlQuery q(db);
    q.prepare(QLatin1String(query));
    for (auto &value : parameters) {
        q.addBindValue(value);
    }

#if DUMP_SQL
    if (query != "SELECT COUNT(*) FROM article WHERE feed_id = ? AND NOT(status  &8)") {
        qDebug() << "Executing query '" << query << "' with ... {";
        for (auto &value : parameters) {
            qDebug() << "    - " << value;
        }
        qDebug() << "} default " << default_value;
    }
#endif

    if (!q.exec()) {
        qCritical() << "SQL error occured !";
        qCritical() << q.lastError().databaseText();
        qCritical() << q.lastError().driverText();
        return default_value;
    }
    if (!q.next()) {
        return default_value;
    }
    return q.value(0).value<T>();
}

bool simpleQuery(const QSqlDatabase &db, const char *query, const std::initializer_list<QVariant> &parameters)
{
#if DUMP_SQL
    qDebug() << "Executing query '" << query << "' with ... {";
    for (auto &value : parameters) {
        qDebug() << "    - " << value;
    }
    qDebug() << "}";
#endif
    QSqlQuery q(db);
    q.prepare(QLatin1String(query));
    for (auto &value : parameters) {
        q.addBindValue(value);
    }
    if (!q.exec()) {
        qCritical() << "SQL error occured !";
        qCritical() << q.lastError().databaseText();
        qCritical() << q.lastError().driverText();
        return false;
    }
    return true;
}
}

namespace Akregator
{
namespace Backend
{
class FeedStorage::FeedStoragePrivate
{
public:
    FeedStoragePrivate()
    {
    }

    int feed_id;
    Storage *mainStorage;
};

FeedStorage::FeedStorage(int feed_id, Storage *main)
    : d(new FeedStoragePrivate)
{
    d->feed_id = feed_id;
    d->mainStorage = main;
}

FeedStorage::~FeedStorage()
{
}

int FeedStorage::unread() const
{
    return simpleQuery(d->mainStorage->database(), "SELECT COUNT(*) FROM article WHERE feed_id = ? AND NOT(status  &8)", 0, {d->feed_id});
}

void FeedStorage::setUnread(int unread)
{
    Q_UNUSED(unread);
    // The SQL backend uses an index to maintain unread by itself.
}

int FeedStorage::totalCount() const
{
    return simpleQuery(d->mainStorage->database(), "SELECT COUNT(*) FROM article WHERE feed_id = ?", 0, {d->feed_id});
}

QDateTime FeedStorage::lastFetch() const
{
    return simpleQuery(d->mainStorage->database(), "SELECT last_fetch FROM feed WHERE id = ?", QDateTime(), {d->feed_id});
}

void FeedStorage::setLastFetch(const QDateTime &lastFetch)
{
    simpleQuery(d->mainStorage->database(), "UPDATE feed SET last_fetch = ? WHERE id = ?", {lastFetch, d->feed_id});
    d->mainStorage->markDirty();
}

QStringList FeedStorage::articles() const
{
    QStringList result;
    QSqlQuery q(d->mainStorage->database());
    q.prepare(QLatin1String("SELECT guid FROM article WHERE feed_id = ?"));
    q.addBindValue(d->feed_id);
    if (q.exec()) {
        // Not all SQL backend handle returning size, sadly
        if (q.size() > 0)
            result.reserve(q.size());

        while (q.next()) {
            result << q.value(0).toString();
        }
    }
    return result;
}

void FeedStorage::addEntry(const QString &guid)
{
    if (contains(guid))
        return;
    simpleQuery(d->mainStorage->database(), "INSERT INTO article (feed_id, guid, status) VALUES (?, ?, 0)", {d->feed_id, guid});
    d->mainStorage->markDirty();
}

bool FeedStorage::contains(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT true FROM article WHERE feed_id = ? AND guid = ?", false, {d->feed_id, guid});
}

void FeedStorage::deleteArticle(const QString &guid)
{
    simpleQuery(d->mainStorage->database(), "DELETE FROM article WHERE guid = ? AND feed_id = ?", {guid, d->feed_id});
    d->mainStorage->markDirty();
}

bool FeedStorage::guidIsHash(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT guid_is_hash FROM article WHERE feed_id = ? AND guid = ?", false, {d->feed_id, guid});
}

bool FeedStorage::guidIsPermaLink(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT guid_is_permalink FROM article WHERE feed_id = ? AND guid = ?", false, {d->feed_id, guid});
}

uint FeedStorage::hash(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT hash FROM article WHERE feed_id = ? AND guid = ?", 0, {d->feed_id, guid});
}

void FeedStorage::setDeleted(const QString &guid)
{
    simpleQuery(d->mainStorage->database(),
                "UPDATE article SET content = '', description = '', title = '', link = '', author_name = '', author_url = '', author_email = '' WHERE guid = ? "
                "AND feed_id = ?",
                {guid, d->feed_id});
    d->mainStorage->markDirty();
}

QString FeedStorage::link(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT link FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

QDateTime FeedStorage::pubDate(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT publication_date FROM article WHERE feed_id = ? AND guid = ?", QDateTime(), {d->feed_id, guid});
}

int FeedStorage::status(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT status FROM article WHERE feed_id = ? AND guid = ?", 0, {d->feed_id, guid});
}

void FeedStorage::setStatus(const QString &guid, int status)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET status = ? WHERE guid = ? AND feed_id = ?", {status, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::article(const QString &guid, uint &hash, QString &title, int &status, QDateTime &pubDate) const
{
    if (DUMP_SQL)
        qDebug() << "Fetching article with guid " << guid;
    QStringList result;
    QSqlQuery q(d->mainStorage->database());
    q.prepare(QLatin1String("SELECT hash, title, status, publication_date FROM article WHERE feed_id = ? AND guid = ?"));
    q.addBindValue(d->feed_id);
    q.addBindValue(guid);
    if (!q.exec())
        return;

    if (!q.next())
        return;

    hash = q.value(0).toInt();
    title = q.value(1).toString();
    status = q.value(2).toInt();
    pubDate = q.value(3).toDateTime();
}

QString FeedStorage::title(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT title FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

QString FeedStorage::description(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT description FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

QString FeedStorage::content(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT content FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

void FeedStorage::setPubDate(const QString &guid, const QDateTime &pubdate)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET publication_date = ? WHERE guid = ? AND feed_id = ?", {pubdate, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setGuidIsHash(const QString &guid, bool isHash)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET guid_is_hash = ? WHERE guid = ? AND feed_id = ?", {isHash, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setLink(const QString &guid, const QString &link)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET link = ? WHERE guid = ? AND feed_id = ?", {link, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setHash(const QString &guid, uint hash)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET hash = ? WHERE guid = ? AND feed_id = ?", {hash, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setTitle(const QString &guid, const QString &title)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET title= ? WHERE guid = ? AND feed_id = ?", {title, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setDescription(const QString &guid, const QString &description)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET description = ? WHERE guid = ? AND feed_id = ?", {description, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setContent(const QString &guid, const QString &content)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET content = ? WHERE guid = ? AND feed_id = ?", {content, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setAuthorName(const QString &guid, const QString &author)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET author_name = ? WHERE guid = ? AND feed_id = ?", {author, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setAuthorUri(const QString &guid, const QString &author)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET author_url = ? WHERE guid = ? AND feed_id = ?", {author, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setAuthorEMail(const QString &guid, const QString &author)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET author_email = ? WHERE guid = ? AND feed_id = ?", {author, guid, d->feed_id});
    d->mainStorage->markDirty();
}

QString FeedStorage::authorName(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT author_name FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

QString FeedStorage::authorUri(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT author_url FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

QString FeedStorage::authorEMail(const QString &guid) const
{
    return simpleQuery(d->mainStorage->database(), "SELECT author_email FROM article WHERE feed_id = ? AND guid = ?", QStringLiteral(""), {d->feed_id, guid});
}

void FeedStorage::setGuidIsPermaLink(const QString &guid, bool isPermaLink)
{
    simpleQuery(d->mainStorage->database(), "UPDATE article SET guid_is_permalink = ? WHERE guid = ? AND feed_id = ?", {isPermaLink, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::setEnclosure(const QString &guid, const QString &url, const QString &type, int length)
{
    simpleQuery(d->mainStorage->database(),
                "UPDATE article SET enclosure_url = ?, enclosure_type = ?, enclosure_length = ? WHERE guid = ? AND feed_id = ?",
                {url, type, length, guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::removeEnclosure(const QString &guid)
{
    simpleQuery(d->mainStorage->database(),
                "UPDATE article SET enclosure_url = NULL, enclosure_type = NULL, enclosure_length = NULL WHERE guid = ? AND feed_id = ?",
                {guid, d->feed_id});
    d->mainStorage->markDirty();
}

void FeedStorage::enclosure(const QString &guid, bool &hasEnclosure, QString &url, QString &type, int &length) const
{
    QSqlQuery q(d->mainStorage->database());
    q.prepare(
        QLatin1String("SELECT enclosure_url, enclosure_type, enclosure_length FROM article WHERE guid = ? AND feed_id = ? AND enclosure_url IS NOT NULL"));
    q.addBindValue(guid);
    q.addBindValue(d->feed_id);
    if (q.exec()) {
        if (!q.next()) {
            hasEnclosure = false;
            return;
        }
        hasEnclosure = true;
        url = q.value(0).toString();
        type = q.value(1).toString();
        length = q.value(2).toInt();
    }
}

void FeedStorage::setCategories(const QString &, const QStringList &categories)
{
    // TODO
}

QStringList FeedStorage::categories(const QString &guid) const
{
    // TODO
    return {};
}
} // namespace Backend
} // namespace Akregator
